create table if not exists Administrators (
	username varchar(20) not null unique,
	password varchar(100) not null,
	status varchar(20),
	primary key (username));


create table if not exists Advertiser (
	advertiserID int(4) not null unique auto_increment,
	Username varchar(20) not null unique,
	Password varchar(100) not null,
	AdvertiserEmail varchar(40) not null unique,
	Balance varchar(20),
	DateCreated timestamp default current_timestamp,
	status varchar(20),
	primary key (advertiserID));



Create table if not exists Location (
	LocationID int(4) not null unique auto_increment,
	Area varchar(20),
	primary key (LocationID));



Create table if not exists Schedule (
	ScheduleID int(4) not null unique auto_increment,
	Time varchar(20) ,
	primary key (ScheduleID));


Create table if not exists SalesHistory (
	SalesID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	primary key (SalesID));



create table if not exists VideoAdvertisment(
	videoAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	VideoURL varchar(100),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (videoAdvertismentID));


create table if not exists ImageAdvertisment(
	ImageAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	ImageURL varchar(100),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (ImageAdvertismentID));


create table if not exists TextAdvertisment(
	TextAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	Title varchar(100),
	Content varchar (200),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (TextAdvertismentID));


 -- sql = "select o.OFFERCODE, s.subjectname , o.time, o.room, s.fees,s.units from subjects s, offerings o, enrolled e where s.courseno = o.courseno and e.OFFERCODE = o.OFFERCODE  and e.studentid ="+"'"+studentno+"'";

-- INSERT INTO `textadvertisment`(`TextAdvertismentID`, `advertiserID`, `Title`, `Content`, `LocationID`, `ScheduleID`, `DateCreated`, `Duration`, `status`) VALUES (default,1,'hi','hello',1,1,default,'6','active')

-- INSERT INTO `advertiser`(`advertiserID`, `Username`, `Password`, `AdvertiserEmail`, `Balance`, `DateCreated`, `status`) VALUES (default,'alice','perlaloo','advertiser@email.com','50',default,'active')

-- update advertiser set Balance=Balance+30 where username='alice'



-- drop table administrators;
-- drop table advertiser;
-- drop table location;
-- drop table schedule;
-- drop table saleshistory;
-- drop table videoadvertisment;
-- drop table imageadvertisment;
-- drop table textadvertisment;
--select a.username, s.time, t.duration, from schedule s, location l, advertiser a, textadvertisment t where s.scheduleid = t.scheduleid and l.locationid = t.locationid and a.advertiserid = t.advertiserid ;

-- select s.time, l.area, a.username from schedule s, location l, advertiser a, textadvertisment t where s.scheduleid = t.scheduleid and l.locationid = t.locationid and a.advertiserid = t.advertiserid and a.username ="alice" ;
-- Create table if not exists Timeslot (
-- 	TimeSlotID int(4) not null unique auto_increment,
-- 	Timeslot Time default '00:00:00',
-- 	primary key (TimeslotID));