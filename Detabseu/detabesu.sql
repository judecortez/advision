create table if not exists Administrators (
	username varchar(20) not null unique,
	password varchar(100) not null,
	status varchar(20),
	primary key (username));


create table if not exists Advertiser (
	advertiserID int(4) not null unique auto_increment,
	Username varchar(20) not null unique,
	Password varchar(100) not null,
	AdvertiserEmail varchar(40) not null unique,
	Balance varchar(20),
	DateCreated timestamp default current_timestamp,
	status varchar(20),
	primary key (advertiserID));



Create table if not exists Location (
	LocationID int(4) not null unique auto_increment,
	Area varchar(20),
	primary key (LocationID));



Create table if not exists Schedule (
	ScheduleID int(4) not null unique auto_increment,
	Time varchar(20) ,
	primary key (ScheduleID));






Create table if not exists SalesHistory (
	SalesID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	primary key (SalesID));



create table if not exists VideoAdvertisment(
	videoAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	VideoURL varchar(100),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (videoAdvertismentID));


create table if not exists ImageAdvertisment(
	ImageAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	ImageURL varchar(100),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (ImageAdvertismentID));


create table if not exists TextAdvertisment(
	TextAdvertismentID int(4) not null unique auto_increment,
	advertiserID varchar(20) references advertiser(advertiserID),
	Title varchar(100),
	Content varchar (200),
	LocationID varchar(20) references Location(LocationID),
	ScheduleID varchar(20) references Schedule(ScheduleID),
	DateCreated timestamp default current_timestamp,
	Duration varchar(20),
	status varchar(20),
	primary key (TextAdvertismentID));
